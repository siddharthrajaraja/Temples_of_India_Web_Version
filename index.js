var express =require('express')
var app=new express()

// All Middle-wares involved here -------->>>>
app.set('view engine','ejs');
app.use('/assets',express.static('assets'));

// Including GET ROUTES ----------------->>>>>
var {homepage,login}=require('./server/getRoutes/get')

app.get('/homepage',homepage);
app.get('/login',login)

// Including POST ROUTES ---------------->>>>>



app.listen(process.env.PORT || 4562 ,()=>{

    console.log("Connected to PORT 4562")
})